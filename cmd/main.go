package main

import (
	"avito/internal/app"
	"context"
	"log"
)

func main() {
	ctx := context.Background()

	a, err := app.New(ctx, "config-example", "json", "./config")
	if err != nil {
		log.Fatalf("failed to init app: %s", err.Error())
	}

	err = a.Run()
	if err != nil {
		log.Fatalf("failed to run app: %s", err.Error())
	}
}

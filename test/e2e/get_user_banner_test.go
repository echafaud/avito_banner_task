package e2e

import (
	"avito/internal/auth/provider"
	"avito/internal/model"
	"avito/internal/service"
	"context"
	"encoding/json"
	"github.com/google/uuid"
	"github.com/steinfletcher/apitest"
	"net/http"
	"testing"
	"time"
)

const (
	url              = "/user_banner"
	userToken        = "user"
	tokenHeader      = "token"
	tagQuery         = "tag_id"
	featureQuery     = "feature_id"
	lastVersionQuery = "use_last_revision"
	baseBanner       = "baseBanner"
	cacheBanner      = "cacheBanner"
	updBanner        = "updBanner"
	inactiveBanner   = "inactiveBanner"
)

func TestGetUserBanner(t *testing.T) {
	t.Run("TestGetUserBanner", Case(func(t *testing.T, testApp *App) {
		claims := provider.UserClaims{
			ID:      uuid.New(),
			IsAdmin: false,
		}

		features := map[string]int{
			baseBanner:     1,
			cacheBanner:    2,
			inactiveBanner: 3,
		}

		tags := map[string][]int{
			baseBanner:     {1, 2, 3},
			cacheBanner:    {4, 5, 6},
			inactiveBanner: {2, 5, 3, 6},
		}

		contents := map[string]string{
			baseBanner:     `{"test": "get_banner", "banner": 1}`,
			cacheBanner:    `{"test": "get_banner", "banner": 2}`,
			updBanner:      `{"test": "get_banner", "banner": 2, "upd": true}`,
			inactiveBanner: `{"test": "get_banner", "banner": 3}`,
		}

		actives := map[string]bool{
			baseBanner:     true,
			cacheBanner:    true,
			inactiveBanner: false,
		}

		var bannerUpd *model.BannerUpdate

		for k, v := range features {
			var content map[string]interface{}
			_ = json.Unmarshal([]byte(contents[k]), &content)
			feature := v
			isActive := actives[k]
			banner := &model.BannerData{
				TagIDs:    tags[k],
				FeatureID: &feature,
				Content:   content,
				IsActive:  &isActive,
			}

			id, _ := testApp.bannerService.Create(testApp.ctx, banner)
			if k == cacheBanner {
				_ = json.Unmarshal([]byte(contents[updBanner]), &banner.Content)
				bannerUpd = &model.BannerUpdate{
					ID:         &id,
					BannerData: banner,
				}
			}

		}

		token, _ := testApp.authProvider.JwtToken(claims, userToken, 1*time.Hour)

		// успешное получение баннера
		GetUserBannerSuccess(t, testApp.server, http.StatusOK, token, contents[baseBanner], "1", "1", "")

		// успешное получение баннера из кеша
		GetUserBannerSuccess(t, testApp.server, http.StatusOK, token, contents[baseBanner], "1", "1", "")

		// успешное принудительное получение новой версии после обновления
		GetUserUpdBannerSuccess(testApp.ctx, t, testApp.server, testApp.bannerService, http.StatusOK, token, contents[cacheBanner], bannerUpd)

		// попытка получить баннер с некорректными данными
		GetUserBannerSuccess(t, testApp.server, http.StatusBadRequest, token, "{\"error\":\"strconv.ParseInt: parsing \\\"test\\\": invalid syntax\"}\n", "test", "test", "test")

		// попытка получить несуществующий баннер
		GetUserBannerSuccess(t, testApp.server, http.StatusNotFound, token, "", "10", "10", "true")

		// попытка получить выключенный баннер
		GetUserBannerSuccess(t, testApp.server, http.StatusForbidden, token, "", "3", "3", "true")

	}))
}

func GetUserBannerSuccess(t *testing.T, handler http.Handler, status int, token string, content string, featureID string, tagID string, useLastVersion string) {
	test := apitest.
		New().
		Handler(handler).
		Get(url).
		Header(tokenHeader, token).
		Query(featureQuery, featureID).
		Query(tagQuery, tagID).
		Query(lastVersionQuery, useLastVersion).
		Expect(t).
		Status(status)
	if content != "" {
		test.Body(content)
	}
	test.End()
}

func GetUserUpdBannerSuccess(ctx context.Context, t *testing.T, handler http.Handler, service service.BannerService, status int, token string, content string, bannerUpd *model.BannerUpdate) {
	GetUserBannerSuccess(t, handler, status, token, content, "2", "4", "")
	_ = service.Update(ctx, bannerUpd)
	updContent, _ := json.Marshal(bannerUpd.Content)
	GetUserBannerSuccess(t, handler, status, token, string(updContent), "2", "4", "true")
}

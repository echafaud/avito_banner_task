package e2e

import (
	"avito/internal/app"
	"avito/internal/auth"
	"avito/internal/ent"
	"avito/internal/service"
	"context"
	"github.com/labstack/echo/v4"
	"github.com/redis/go-redis/v9"
	"testing"
)

type App struct {
	ctx           context.Context
	server        *echo.Echo
	bannerService service.BannerService
	dbClient      *ent.Client
	redisClient   *redis.Client
	authProvider  auth.Provider
}

func New(t *testing.T) *App {
	ctx := context.Background()
	testApp, err := app.New(ctx, "config-example", "json", "../../config")
	if err != nil {
		t.Fatalf("failed to init app: %s", err.Error())
	}
	return &App{
		ctx:           ctx,
		server:        testApp.Server(),
		bannerService: testApp.BannerService(),
		dbClient:      testApp.DBClient(),
		redisClient:   testApp.Redis(),
		authProvider:  testApp.Auth(),
	}
}
func Case(testCase func(t *testing.T, testApp *App)) func(t *testing.T) {
	return func(t *testing.T) {
		testApp := New(t)
		defer testApp.afterEach(t)
		testCase(t, testApp)
	}
}

func (ta *App) afterEach(t *testing.T) {
	_, err := ta.dbClient.ExecContext(ta.ctx, "TRUNCATE TABLE banners CASCADE")
	if err != nil {
		t.Fatalf("failed to drop table %s", err.Error())
	}
	err = ta.redisClient.FlushDB(ta.ctx).Err()
	if err != nil {
		t.Fatalf("failed to drop redis %s", err.Error())
	}
}

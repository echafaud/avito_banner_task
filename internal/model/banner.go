package model

import (
	"github.com/google/uuid"
	"time"
)

type Banner struct {
	ID        int                    `json:"banner_id"`
	TagIDs    []int                  `json:"tag_ids"`
	FeatureID int                    `json:"feature_id"`
	Content   map[string]interface{} `json:"content"`
	IsActive  bool                   `json:"is_active"`
	CreatedAt time.Time              `json:"created_at"`
	UpdatedAt *time.Time             `json:"updated_at"`
}

type BannerVersion struct {
	ID        uuid.UUID              `json:"version_id"`
	TagIDs    []int                  `json:"tag_ids"`
	FeatureID int                    `json:"feature_id"`
	Version   int                    `json:"version"`
	Content   map[string]interface{} `json:"content"`
	CreatedAt time.Time              `json:"created_at"`
}

type BannerData struct {
	TagIDs    []int                  `json:"tag_ids" validate:"omitnil" err:"tag_ids"`
	FeatureID *int                   `json:"feature_id" validate:"omitnil" err:"feature_id"`
	Content   map[string]interface{} `json:"content" validate:"omitnil" err:"content"`
	IsActive  *bool                  `json:"is_active" validate:"omitnil" err:"is_active"`
}

type BannerCreate struct {
	*BannerData `validate:"nestedAllRequired"`
}

type BannerIdentifier struct {
	TagID     *int `query:"tag_id" validate:"omitnil" err:"tag_id"`
	FeatureID *int `query:"feature_id" validate:"omitnil" err:"feature_id"`
}

type BannerGet struct {
	Banner         BannerIdentifier `validate:"nestedAllRequired"`
	UseLastVersion bool             `query:"use_last_revision" validate:"omitempty" err:"use_last_revision"`
}

type BannerUpdate struct {
	ID          *int `param:"id" validate:"required" err:"id"`
	*BannerData `validate:"nestedAnyRequired"`
}

type BannerDelete struct {
	ID *int `param:"id" validate:"required" err:"id"`
}

type UpdateActiveVersion struct {
	ID       *int      `param:"id" validate:"required" err:"id"`
	ActiveID uuid.UUID `param:"version_id" validate:"required" err:"id"`
}

type Limits struct {
	Limit  int `query:"limit" validate:"omitempty,gte=0" err:"limit"`
	Offset int `query:"offset" validate:"omitempty,gte=0" err:"offset"`
}

type BannerGetByFilter struct {
	Banner BannerIdentifier `validate:"nestedAnyRequired"`
	Limits Limits
}

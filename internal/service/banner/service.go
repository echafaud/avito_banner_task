package banner

import (
	"avito/internal/repository"
)

const versionsCnt = 3

type service struct {
	bannerRepository repository.BannerRepository
}

func New(bannerRepository repository.BannerRepository) *service {
	return &service{
		bannerRepository: bannerRepository,
	}
}

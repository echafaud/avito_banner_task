package banner

import (
	"avito/internal/converter"
	customErrors "avito/internal/error/banner"
	"avito/internal/model"
	"context"
)

func (s *service) Get(ctx context.Context, featureID int, tagID int) (map[string]interface{}, error) {
	banner, err := s.bannerRepository.Get(ctx, featureID, tagID)
	if err != nil {
		return nil, err
	}
	if !banner.Edges.MainBanner.IsActive {
		return nil, customErrors.AccessDenied
	}
	return banner.Content, nil
}

func (s *service) GetByFilter(ctx context.Context, filter *model.BannerGetByFilter) ([]*model.Banner, error) {
	filter.Limits = converter.ToDefaultLimits(filter.Limits)
	content, err := s.bannerRepository.GetByFilter(ctx, filter)

	if err != nil {
		return nil, err
	}
	return content, nil
}

func (s *service) GetVersions(ctx context.Context, id int) ([]*model.BannerVersion, error) {
	versions, err := s.bannerRepository.GetVersions(ctx, id)
	if err != nil {
		return nil, err
	}
	convVers := make([]*model.BannerVersion, len(versions), cap(versions))
	for i, ver := range versions {
		convVers[i] = converter.ToPrettyBannerVersion(ver)
	}
	return convVers, nil
}

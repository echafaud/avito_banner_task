package banner

import (
	customErrors "avito/internal/error/banner"
	"avito/internal/model"
	repo "avito/internal/repository/banner"
	"context"
)

func (s *service) Create(ctx context.Context, data *model.BannerData) (int, error) {

	isExist, err := s.bannerRepository.Exists(ctx, *data.FeatureID, data.TagIDs, repo.NewExistsOptions())
	if err != nil {
		return 0, err
	}

	if isExist {
		return 0, customErrors.InternalError
	}

	id, err := s.bannerRepository.Create(ctx, data)
	if err != nil {
		return 0, err
	}
	return id, nil
}

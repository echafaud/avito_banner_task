package banner

import (
	"avito/internal/converter"
	bannerVersionTag "avito/internal/ent/bannerversiontag"
	customErrors "avito/internal/error/banner"
	"avito/internal/model"
	repo "avito/internal/repository/banner"
	"context"
)

func (s *service) Update(ctx context.Context, data *model.BannerUpdate) error {
	currBann, err := s.bannerRepository.GetMainBanner(ctx, *data.ID)

	if err != nil {
		return err
	}
	currVer, err := s.bannerRepository.GetVersionBanner(ctx, currBann.ActiveVersionID)
	lastVer, err := s.bannerRepository.GetVersionBanner(ctx, currBann.LatestVersionID)
	if err != nil {
		return err
	}
	convertedBann, err := converter.ToBannerDataFromBannerUpdate(ctx, *data.BannerData, currVer, currBann)
	if err != nil {
		return err
	}
	opt := repo.NewExistsOptions()
	opt.Exception = &currBann.ID
	isExist, err := s.bannerRepository.Exists(ctx, *convertedBann.FeatureID, convertedBann.TagIDs, opt)
	if err != nil {
		return err
	}
	if isExist {
		return customErrors.InternalError
	}
	data.BannerData = convertedBann
	err = s.bannerRepository.Update(ctx, currBann, data, lastVer.Version, versionsCnt)
	if err != nil {
		return err
	}
	return nil
}

func (s *service) UpdateActiveVersion(ctx context.Context, data *model.UpdateActiveVersion) error {
	updVer, err := s.bannerRepository.GetVersionBanner(ctx, data.ActiveID)
	if err != nil {
		return err
	}
	if updVer.BannerID != *data.ID {
		return customErrors.InternalError
	}
	var tagIDs []int
	err = updVer.QueryBannerVersionTags().Select(bannerVersionTag.FieldTagID).Scan(ctx, &tagIDs)
	if err != nil {
		return err
	}
	opt := repo.NewExistsOptions()
	opt.Exception = &updVer.BannerID
	isExist, err := s.bannerRepository.Exists(ctx, updVer.FeatureID, tagIDs, opt)
	if err != nil {
		return err
	}
	if isExist {
		return customErrors.InternalError
	}
	err = s.bannerRepository.UpdateActiveVersion(ctx, data)
	if err != nil {
		return err
	}
	return nil
}

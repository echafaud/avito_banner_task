package service

import (
	"avito/internal/model"
	"context"
)

type BannerService interface {
	Create(ctx context.Context, data *model.BannerData) (int, error)
	Get(ctx context.Context, featureID int, tagID int) (map[string]interface{}, error)
	GetByFilter(ctx context.Context, filter *model.BannerGetByFilter) ([]*model.Banner, error)
	GetVersions(ctx context.Context, id int) ([]*model.BannerVersion, error)
	Update(ctx context.Context, data *model.BannerUpdate) error
	UpdateActiveVersion(ctx context.Context, data *model.UpdateActiveVersion) error
	Delete(ctx context.Context, id int) error
}

package converter

import (
	"avito/internal/ent"
	bannerVersionTag "avito/internal/ent/bannerversiontag"
	"avito/internal/model"
	"context"
)

const (
	defLimit  = 10
	defOffset = 0
)

func ToBannerDataFromBannerUpdate(ctx context.Context, data model.BannerData, currVer *ent.BannerVersion, currBann *ent.Banner) (*model.BannerData, error) {
	tagIDs := data.TagIDs
	featureId := data.FeatureID
	content := data.Content
	isActive := data.IsActive

	if content == nil {
		content = currVer.Content
	}

	if featureId == nil {
		featureId = &currVer.FeatureID
	}

	if isActive == nil {
		isActive = &currBann.IsActive
	}

	if tagIDs == nil {
		err := currVer.QueryBannerVersionTags().Select(bannerVersionTag.FieldTagID).Scan(ctx, &tagIDs)
		if err != nil {
			return nil, err
		}
	}
	return &model.BannerData{
		TagIDs:    tagIDs,
		FeatureID: featureId,
		Content:   content,
		IsActive:  isActive,
	}, nil
}

func ToDefaultLimits(limits model.Limits) model.Limits {
	l := limits.Limit
	o := limits.Offset

	if l == 0 {
		l = defLimit
	}
	if o == 0 {
		o = defOffset
	}
	return model.Limits{Limit: l, Offset: o}
}

func ToBannerFromBannerVersion(version *ent.BannerVersion) *model.Banner {
	tagIDs := make([]int, len(version.Edges.BannerVersionTags), cap(version.Edges.BannerVersionTags))
	for i, tag := range version.Edges.BannerVersionTags {
		tagIDs[i] = tag.TagID
	}
	return &model.Banner{
		ID:        version.BannerID,
		TagIDs:    tagIDs,
		FeatureID: version.FeatureID,
		Content:   version.Content,
		IsActive:  version.Edges.MainBanner.IsActive,
		CreatedAt: version.Edges.MainBanner.CreatedAt,
		UpdatedAt: version.Edges.MainBanner.UpdatedAt,
	}
}

func ToPrettyBannerVersion(version *ent.BannerVersion) *model.BannerVersion {
	tagIDs := make([]int, len(version.Edges.BannerVersionTags), cap(version.Edges.BannerVersionTags))
	for i, tag := range version.Edges.BannerVersionTags {
		tagIDs[i] = tag.TagID
	}
	return &model.BannerVersion{
		ID:        version.ID,
		TagIDs:    tagIDs,
		Version:   version.Version,
		FeatureID: version.FeatureID,
		Content:   version.Content,
		CreatedAt: version.CreatedAt,
	}
}

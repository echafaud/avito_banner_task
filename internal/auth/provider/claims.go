package provider

import (
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
)

type UserClaims struct {
	ID      uuid.UUID `json:"id"`
	IsAdmin bool      `json:"admin"`
}

type JwtUserClaims struct {
	UserClaims
	jwt.RegisteredClaims
}

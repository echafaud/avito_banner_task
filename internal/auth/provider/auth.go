package provider

import (
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"time"
)

type Config struct {
	SigningKeys   map[string]string
	SigningMethod string
}

type authProvider struct {
	config  Config
	keyFunc func(token *jwt.Token) (interface{}, error)
}

func New(conf Config) *authProvider {
	signingKeys := make(map[string][]byte)
	for k, v := range conf.SigningKeys {
		signingKeys[k] = []byte(v)
	}

	keyFunc := func(token *jwt.Token) (interface{}, error) {
		if token.Method.Alg() != conf.SigningMethod {
			return nil, &echojwt.TokenError{Token: token, Err: fmt.Errorf("unexpected jwt signing method=%v", token.Header["alg"])}
		}
		if kid, ok := token.Header["kid"].(string); ok {
			if key, ok := signingKeys[kid]; ok {
				return key, nil
			}
		}
		return nil, &echojwt.TokenError{Token: token, Err: fmt.Errorf("unexpected jwt key id=%v", token.Header["kid"])}
	}
	return &authProvider{
		config:  conf,
		keyFunc: keyFunc,
	}
}

func (a *authProvider) tokenParseFunc(isAdmin bool, keyFunc func(token *jwt.Token) (interface{}, error)) func(c echo.Context, auth string) (interface{}, error) {
	return func(c echo.Context, auth string) (interface{}, error) {
		token, err := jwt.ParseWithClaims(auth, new(JwtUserClaims), keyFunc)
		if err != nil {
			return nil, &echojwt.TokenError{Token: token, Err: err}
		}
		claims := token.Claims.(*JwtUserClaims)
		if !token.Valid || claims.IsAdmin != isAdmin {
			return nil, &echojwt.TokenError{Token: token, Err: errors.New("invalid token")}
		}
		c.Set("userClaims", claims) // прокидываем пользователя в контекст (оставляем возможность проверять роль уже внутри)
		return token, nil
	}
}

func (a *authProvider) SetupMW(isAdmin bool) echojwt.Config {
	return echojwt.Config{
		TokenLookup:    "header:token",
		ParseTokenFunc: a.tokenParseFunc(isAdmin, a.keyFunc),
	}
}

func (a *authProvider) JwtToken(userClaims UserClaims, tokenType string, duration time.Duration) (string, error) {
	claims := JwtUserClaims{
		UserClaims: userClaims,
		RegisteredClaims: jwt.RegisteredClaims{
			ID:        uuid.New().String(),
			Subject:   userClaims.ID.String(),
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(duration)),
			NotBefore: jwt.NewNumericDate(time.Now()),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
		}}
	jwtClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	jwtClaims.Header["kid"] = tokenType
	token, err := jwtClaims.SignedString([]byte(a.config.SigningKeys[tokenType]))
	if err != nil {
		return "", err
	}
	return token, nil
}

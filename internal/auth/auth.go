package auth

import (
	"avito/internal/auth/provider"
	echojwt "github.com/labstack/echo-jwt/v4"
	"time"
)

type Provider interface {
	SetupMW(isAdmin bool) echojwt.Config
	JwtToken(userClaims provider.UserClaims, tokenType string, duration time.Duration) (string, error)
}

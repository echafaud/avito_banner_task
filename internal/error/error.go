package error

import (
	"avito/internal/error/banner"
	"fmt"
	"github.com/labstack/echo/v4"
)

type HTTPError struct {
	Code int    `json:"-"`
	Err  string `json:"error"`
}

func NewHTTPError(c echo.Context, err error) error {
	custErr := banner.ToCustomErrorFromAny(err)
	if custErr.NoContent {
		return c.NoContent(custErr.Code)
	}
	return c.JSON(custErr.Code, echo.Map{"error": custErr.Error()})
}

func (he *HTTPError) Error() string {
	return fmt.Sprintf("code=%d, message=%v", he.Code, he.Err)
}

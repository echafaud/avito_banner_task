package banner

import (
	"avito/internal/ent"
	"errors"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"net/http"
	"reflect"
)

const (
	requiredTag       = "required"
	nestedAllRequired = "nestedAllRequired"
	nestedAnyRequired = "nestedAnyRequired"
)

func ToCustomErrorFromValidator(err validator.FieldError) error {
	var cerr error
	switch err.Tag() {
	case nestedAllRequired:
		f := reflect.ValueOf(err.Value())
		for i := 0; i < f.NumField(); i++ {
			if f.Field(i).IsNil() {
				cerr = &CustomError{
					Code: http.StatusBadRequest,
					Err: &MissingError{
						ValueError: ValueError{Names: []string{f.Type().Field(i).Tag.Get("err")}},
					}}
				break
			}
		}
	case nestedAnyRequired:
		f := reflect.ValueOf(err.Value())
		n := make([]string, f.NumField(), f.NumField())
		for i := 0; i < f.NumField(); i++ {
			if f.Field(i).IsNil() {
				n[i] = f.Type().Field(i).Tag.Get("err")
			}
		}
		cerr = &CustomError{
			Code: http.StatusBadRequest,
			Err: &MissingError{
				ValueError: ValueError{Names: n},
			},
		}
	default:
		cerr = UnknownError
	}
	return cerr
}

func ToCustomErrorFromAny(err error) *CustomError {
	var httpErr *echo.HTTPError
	if ok := errors.As(err, &httpErr); ok {
		return &CustomError{
			Code: httpErr.Code,
			Msg:  httpErr.Message.(string),
			Err:  httpErr,
		}
	}
	var customErr *CustomError
	if ok := errors.As(err, &customErr); ok {
		return customErr
	}

	var entError *ent.NotFoundError
	if ok := errors.As(err, &entError); ok {
		return NotFoundError
	}

	return UnknownError
}

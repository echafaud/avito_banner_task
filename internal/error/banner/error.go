package banner

import (
	"net/http"
	"strings"
)

type (
	ValueError struct {
		Names []string
	}

	MissingError struct {
		ValueError
	}

	CustomError struct {
		Code      int
		Msg       string
		Err       error
		NoContent bool
	}
)

var (
	AccessDenied  = &CustomError{Code: http.StatusForbidden, NoContent: true}
	NotFoundError = &CustomError{Code: http.StatusNotFound, NoContent: true}
	InternalError = &CustomError{Code: http.StatusInternalServerError, NoContent: true}
	UnknownError  = &CustomError{Code: http.StatusInternalServerError, NoContent: true}
)

func (e *MissingError) Error() string {
	if len(e.Names) == 1 {
		return "The " + e.ValueError.Names[0] + " field is required"
	}
	return "It is required to fill in at least one of the following fields: " + strings.Join(e.Names, ", ")
}

func (e *CustomError) Error() string {
	if e.Msg != "" {
		return e.Msg
	}
	return e.Err.Error()
}

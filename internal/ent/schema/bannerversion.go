package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
	"time"
)

// BannerVersion holds the schema definition for the BannerVersion entity.
type BannerVersion struct {
	ent.Schema
}

// Fields of the BannerVersion.
func (BannerVersion) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).
			Unique(),
		field.Int("feature_id"),
		field.Int("banner_id"),
		field.JSON("content", map[string]any{}),
		field.Int("version"),
		field.Time("created_at").Default(time.Now),
	}
}

// Edges of the BannerVersion.
func (BannerVersion) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("tags", Tag.Type).Through("banner_version_tags", BannerVersionTag.Type).Annotations(entsql.OnDelete(entsql.Cascade)),
		edge.From("v_feature", Feature.Type).
			Ref("banner_vers").
			Field("feature_id").
			Unique().
			Required(),
		edge.From("main_banner", Banner.Type).Ref("versions").Unique().Required().Field("banner_id"),
	}
}

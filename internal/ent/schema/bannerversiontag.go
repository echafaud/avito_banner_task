package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// BannerVersionTag holds the schema definition for the BannerVersionTag entity.
type BannerVersionTag struct {
	ent.Schema
}

func (BannerVersionTag) Annotations() []schema.Annotation {
	return []schema.Annotation{
		field.ID("banner_version_id", "tag_id"),
	}
}

// Fields of the BannerVersionTag.
func (BannerVersionTag) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("banner_version_id", uuid.UUID{}),
		field.Int("tag_id"),
	}
}

// Edges of the BannerVersionTag.
func (BannerVersionTag) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("banner_version", BannerVersion.Type).
			Unique().
			Required().
			Field("banner_version_id").Annotations(entsql.OnDelete(entsql.Cascade)),
		edge.To("tag", Tag.Type).
			Unique().
			Required().
			Field("tag_id").Annotations(entsql.OnDelete(entsql.Cascade)),
	}
}

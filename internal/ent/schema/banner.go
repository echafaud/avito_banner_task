package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
	"time"
)

// Banner holds the schema definition for the Banner entity.
type Banner struct {
	ent.Schema
}

// Fields of the Banner.
func (Banner) Fields() []ent.Field {
	return []ent.Field{
		field.Int("id").
			Unique(),
		field.UUID("active_version_id", uuid.UUID{}),
		field.UUID("latest_version_id", uuid.UUID{}),
		field.Time("created_at").Default(time.Now),
		field.Time("updated_at").Nillable().Optional(),
		field.Bool("is_active"),
	}
}

// Edges of the Banner.
func (Banner) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("versions", BannerVersion.Type).Annotations(entsql.OnDelete(entsql.Cascade)),
	}
}

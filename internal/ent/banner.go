// Code generated by ent, DO NOT EDIT.

package ent

import (
	"avito/internal/ent/banner"
	"fmt"
	"strings"
	"time"

	"entgo.io/ent"
	"entgo.io/ent/dialect/sql"
	"github.com/google/uuid"
)

// Banner is the model entity for the Banner schema.
type Banner struct {
	config `json:"-"`
	// ID of the ent.
	ID int `json:"id,omitempty"`
	// ActiveVersionID holds the value of the "active_version_id" field.
	ActiveVersionID uuid.UUID `json:"active_version_id,omitempty"`
	// LatestVersionID holds the value of the "latest_version_id" field.
	LatestVersionID uuid.UUID `json:"latest_version_id,omitempty"`
	// CreatedAt holds the value of the "created_at" field.
	CreatedAt time.Time `json:"created_at,omitempty"`
	// UpdatedAt holds the value of the "updated_at" field.
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
	// IsActive holds the value of the "is_active" field.
	IsActive bool `json:"is_active,omitempty"`
	// Edges holds the relations/edges for other nodes in the graph.
	// The values are being populated by the BannerQuery when eager-loading is set.
	Edges        BannerEdges `json:"edges"`
	selectValues sql.SelectValues
}

// BannerEdges holds the relations/edges for other nodes in the graph.
type BannerEdges struct {
	// Versions holds the value of the versions edge.
	Versions []*BannerVersion `json:"versions,omitempty"`
	// loadedTypes holds the information for reporting if a
	// type was loaded (or requested) in eager-loading or not.
	loadedTypes [1]bool
}

// VersionsOrErr returns the Versions value or an error if the edge
// was not loaded in eager-loading.
func (e BannerEdges) VersionsOrErr() ([]*BannerVersion, error) {
	if e.loadedTypes[0] {
		return e.Versions, nil
	}
	return nil, &NotLoadedError{edge: "versions"}
}

// scanValues returns the types for scanning values from sql.Rows.
func (*Banner) scanValues(columns []string) ([]any, error) {
	values := make([]any, len(columns))
	for i := range columns {
		switch columns[i] {
		case banner.FieldIsActive:
			values[i] = new(sql.NullBool)
		case banner.FieldID:
			values[i] = new(sql.NullInt64)
		case banner.FieldCreatedAt, banner.FieldUpdatedAt:
			values[i] = new(sql.NullTime)
		case banner.FieldActiveVersionID, banner.FieldLatestVersionID:
			values[i] = new(uuid.UUID)
		default:
			values[i] = new(sql.UnknownType)
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the Banner fields.
func (b *Banner) assignValues(columns []string, values []any) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case banner.FieldID:
			value, ok := values[i].(*sql.NullInt64)
			if !ok {
				return fmt.Errorf("unexpected type %T for field id", value)
			}
			b.ID = int(value.Int64)
		case banner.FieldActiveVersionID:
			if value, ok := values[i].(*uuid.UUID); !ok {
				return fmt.Errorf("unexpected type %T for field active_version_id", values[i])
			} else if value != nil {
				b.ActiveVersionID = *value
			}
		case banner.FieldLatestVersionID:
			if value, ok := values[i].(*uuid.UUID); !ok {
				return fmt.Errorf("unexpected type %T for field latest_version_id", values[i])
			} else if value != nil {
				b.LatestVersionID = *value
			}
		case banner.FieldCreatedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field created_at", values[i])
			} else if value.Valid {
				b.CreatedAt = value.Time
			}
		case banner.FieldUpdatedAt:
			if value, ok := values[i].(*sql.NullTime); !ok {
				return fmt.Errorf("unexpected type %T for field updated_at", values[i])
			} else if value.Valid {
				b.UpdatedAt = new(time.Time)
				*b.UpdatedAt = value.Time
			}
		case banner.FieldIsActive:
			if value, ok := values[i].(*sql.NullBool); !ok {
				return fmt.Errorf("unexpected type %T for field is_active", values[i])
			} else if value.Valid {
				b.IsActive = value.Bool
			}
		default:
			b.selectValues.Set(columns[i], values[i])
		}
	}
	return nil
}

// Value returns the ent.Value that was dynamically selected and assigned to the Banner.
// This includes values selected through modifiers, order, etc.
func (b *Banner) Value(name string) (ent.Value, error) {
	return b.selectValues.Get(name)
}

// QueryVersions queries the "versions" edge of the Banner entity.
func (b *Banner) QueryVersions() *BannerVersionQuery {
	return NewBannerClient(b.config).QueryVersions(b)
}

// Update returns a builder for updating this Banner.
// Note that you need to call Banner.Unwrap() before calling this method if this Banner
// was returned from a transaction, and the transaction was committed or rolled back.
func (b *Banner) Update() *BannerUpdateOne {
	return NewBannerClient(b.config).UpdateOne(b)
}

// Unwrap unwraps the Banner entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (b *Banner) Unwrap() *Banner {
	_tx, ok := b.config.driver.(*txDriver)
	if !ok {
		panic("ent: Banner is not a transactional entity")
	}
	b.config.driver = _tx.drv
	return b
}

// String implements the fmt.Stringer.
func (b *Banner) String() string {
	var builder strings.Builder
	builder.WriteString("Banner(")
	builder.WriteString(fmt.Sprintf("id=%v, ", b.ID))
	builder.WriteString("active_version_id=")
	builder.WriteString(fmt.Sprintf("%v", b.ActiveVersionID))
	builder.WriteString(", ")
	builder.WriteString("latest_version_id=")
	builder.WriteString(fmt.Sprintf("%v", b.LatestVersionID))
	builder.WriteString(", ")
	builder.WriteString("created_at=")
	builder.WriteString(b.CreatedAt.Format(time.ANSIC))
	builder.WriteString(", ")
	if v := b.UpdatedAt; v != nil {
		builder.WriteString("updated_at=")
		builder.WriteString(v.Format(time.ANSIC))
	}
	builder.WriteString(", ")
	builder.WriteString("is_active=")
	builder.WriteString(fmt.Sprintf("%v", b.IsActive))
	builder.WriteByte(')')
	return builder.String()
}

// Banners is a parsable slice of Banner.
type Banners []*Banner

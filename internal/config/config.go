package config

import (
	"github.com/spf13/viper"
)

type db struct {
	Host     string `mapstructure:"host"`
	Port     string `mapstructure:"port"`
	Name     string `mapstructure:"name"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
}

type auth struct {
	SigningKeys   map[string]string `mapstructure:"signingKeys"`
	SigningMethod string            `mapstructure:"signingMethod"`
}
type server struct {
	Address string `mapstructure:"address"`
}
type redis struct {
	Address  string `mapstructure:"address"`
	Password string `mapstructure:"password"`
	DB       int    `mapstructure:"db"`
}
type dbData struct {
	TagsCnt     int `mapstructure:"tagsCount"`
	FeaturesCnt int `mapstructure:"featuresCount"`
	BannersCnt  int `mapstructure:"bannersCount"`
}

type Config struct {
	DbData dbData `mapstructure:"dbData"`
	Db     db     `mapstructure:"db"`
	Auth   auth   `mapstructure:"auth"`
	Serv   server `mapstructure:"serv"`
	Redis  redis  `mapstructure:"red"`
}

func New() (*Config, error) {
	var conf Config
	err := vp.ReadInConfig()

	if err != nil {
		return nil, err
	}

	err = vp.Unmarshal(&conf)
	if err != nil {
		return nil, err
	}

	return &conf, nil
}

var vp *viper.Viper

func Init(configName, configType, configPath string) {
	vp = viper.New()
	vp.SetConfigName(configName)
	vp.SetConfigType(configType)
	vp.AddConfigPath(configPath)
}

package banner

import (
	"avito/internal/ent"
	"avito/internal/ent/banner"
	bannerVersion "avito/internal/ent/bannerversion"
	"context"
)

func (r *repository) Delete(ctx context.Context, id int) error {
	err := r.WithTx(ctx, func(tx *ent.Tx) error {
		client := tx.Client()
		opt := Options{Tx: client}

		err := r.DeleteBanner(ctx, id, opt)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return err
	}

	return err
}

func (r *repository) DeleteBanner(ctx context.Context, id int, options Options) error {
	client := r.client
	if options.Tx != nil {
		client = options.Tx
	}
	_, err := client.Banner.Delete().Where(banner.ID(id)).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *repository) DeleteVersion(ctx context.Context, id int, version int, options Options) error {
	client := r.client
	if options.Tx != nil {
		client = options.Tx
	}
	_, err := client.Debug().BannerVersion.Delete().Where(bannerVersion.And(bannerVersion.Version(version), bannerVersion.BannerID(id))).Exec(ctx)
	if err != nil {
		return err
	}
	return nil
}

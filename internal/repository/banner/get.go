package banner

import (
	"avito/internal/converter"
	"avito/internal/ent"
	"avito/internal/ent/banner"
	bannerVersion "avito/internal/ent/bannerversion"
	bannerVersionTag "avito/internal/ent/bannerversiontag"
	"avito/internal/model"
	"context"
	"entgo.io/ent/dialect/sql"
	"github.com/google/uuid"
)

func (r *repository) Get(ctx context.Context, featureID int, tagID int) (*ent.BannerVersion, error) {
	bann, err := r.client.BannerVersion.Query().Where(func(s *sql.Selector) {
		bann := sql.Table(banner.Table)
		bannTags := sql.Table(bannerVersionTag.Table)
		s.Join(bann).On(s.C(bannerVersion.FieldID), s.C(bann.C(banner.FieldActiveVersionID)))
		s.Join(bannTags).On(s.C(bannerVersion.FieldID), bannTags.C(bannerVersionTag.FieldBannerVersionID))
		s.Where(sql.And(sql.EQ(s.C(bannerVersion.FieldFeatureID), featureID), sql.EQ(bannTags.C(bannerVersionTag.FieldTagID), tagID)))
	}).WithMainBanner(func(q *ent.BannerQuery) {
		q.Select(banner.FieldIsActive)
	}).Only(ctx)
	if err != nil {
		return nil, err
	}
	return bann, nil
}

func (r *repository) GetByFilter(ctx context.Context, filter *model.BannerGetByFilter) ([]*model.Banner, error) {
	banns, err := r.client.Debug().BannerVersion.Query().Where(func(s *sql.Selector) {
		bann := sql.Table(banner.Table)
		bannTags := sql.Table(bannerVersionTag.Table)
		s.Join(bann).On(s.C(bannerVersion.FieldID), s.C(bann.C(banner.FieldActiveVersionID)))
		s.Join(bannTags).On(s.C(bannerVersion.FieldID), bannTags.C(bannerVersionTag.FieldBannerVersionID))
		if filter.Banner.FeatureID != nil {
			s.Where(sql.EQ(s.C(bannerVersion.FieldFeatureID), *filter.Banner.FeatureID))
		}
		if filter.Banner.TagID != nil {
			s.Where(sql.EQ(bannTags.C(bannerVersionTag.FieldTagID), filter.Banner.TagID))
		}
		s.Distinct()
	}).WithMainBanner(func(q *ent.BannerQuery) {
		q.Select(banner.FieldID, banner.FieldIsActive, banner.FieldCreatedAt, banner.FieldUpdatedAt)
	}).WithBannerVersionTags(func(q *ent.BannerVersionTagQuery) {
		q.Select(bannerVersionTag.FieldTagID)
	}).Limit(filter.Limits.Limit).Offset(filter.Limits.Offset).All(ctx)
	if err != nil {
		return nil, err
	}
	filteredBanns := make([]*model.Banner, len(banns), cap(banns))
	for i, bann := range banns {
		filteredBanns[i] = converter.ToBannerFromBannerVersion(bann)
	}

	return filteredBanns, nil
}

func (r *repository) Exists(ctx context.Context, featureID int, tagIDs []int, opt *ExistsOpt) (bool, error) {
	tags := make([]any, len(tagIDs))
	for i, v := range tagIDs {
		tags[i] = v
	}
	bann, err := r.client.Banner.Query().Where(func(s *sql.Selector) {
		bannVer := sql.Table(bannerVersion.Table)
		bannTags := sql.Table(bannerVersionTag.Table)
		s.Join(bannVer).On(bannVer.C(bannerVersion.FieldID), s.C(banner.FieldActiveVersionID))
		s.Join(bannTags).On(bannVer.C(bannerVersion.FieldID), bannTags.C(bannerVersionTag.FieldBannerVersionID))
		s.Where(sql.And(sql.EQ(bannerVersion.FieldFeatureID, featureID), sql.In(bannTags.C(bannerVersionTag.FieldTagID), tags...)))
		if opt.Exception != nil {
			s.Where(sql.NEQ(s.C(banner.FieldID), *opt.Exception))
		}
		s.Distinct()
	}).Exist(ctx)
	if err != nil {
		return false, err
	}
	return bann, nil
}

func (r *repository) GetMainBanner(ctx context.Context, id int) (*ent.Banner, error) {
	bann, err := r.client.Banner.Get(ctx, id)
	if err != nil {
		return nil, err
	}
	return bann, nil
}

func (r *repository) GetVersionBanner(ctx context.Context, id uuid.UUID) (*ent.BannerVersion, error) {
	bann, err := r.client.BannerVersion.Get(ctx, id)
	if err != nil {
		return nil, err
	}
	return bann, nil
}
func (r *repository) GetVersions(ctx context.Context, id int) ([]*ent.BannerVersion, error) {
	vers, err := r.client.BannerVersion.Query().Where(bannerVersion.BannerID(id)).
		WithBannerVersionTags(func(q *ent.BannerVersionTagQuery) {
			q.Select(bannerVersionTag.FieldTagID)
		}).
		All(ctx)
	if err != nil {
		return nil, err
	}
	return vers, err
}

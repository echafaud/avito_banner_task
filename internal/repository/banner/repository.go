package banner

import (
	"avito/internal/ent"
	"context"
	"fmt"
)

type repository struct {
	client *ent.Client
}

func New(client *ent.Client) *repository {
	return &repository{client: client}
}

func (r *repository) WithTx(ctx context.Context, fn func(tx *ent.Tx) error) error {
	tx, err := r.client.Tx(ctx)
	if err != nil {
		return err
	}
	defer func() {
		if v := recover(); v != nil {
			tx.Rollback()
			panic(v)
		}
	}()
	if err := fn(tx); err != nil {
		if rerr := tx.Rollback(); rerr != nil {
			err = fmt.Errorf("%w: rolling back transaction: %v", err, rerr)
		}
		return err
	}
	if err := tx.Commit(); err != nil {
		return fmt.Errorf("committing transaction: %w", err)
	}
	return nil
}

type Options struct {
	Tx *ent.Client
}

type ExistsOpt struct {
	Exception *int
}

func (r *repository) NewExistsOptions() *ExistsOpt {
	return &ExistsOpt{}
}
func (r *repository) NewOptions() *Options {
	return &Options{}
}

func NewExistsOptions() *ExistsOpt {
	return &ExistsOpt{}
}
func NewOptions() *Options {
	return &Options{}
}

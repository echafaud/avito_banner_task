package banner

import (
	"avito/internal/ent"
	"avito/internal/model"
	"context"
	"github.com/google/uuid"
)

func (r *repository) Update(ctx context.Context, currBann *ent.Banner, data *model.BannerUpdate, currVers int, versCnt int) error {
	err := r.WithTx(ctx, func(tx *ent.Tx) error {
		client := tx.Client()
		opt := Options{Tx: client}
		bannerVersionID := uuid.New()
		bannerVersion, err := r.createBannerVersion(ctx, *data.ID, bannerVersionID, currVers+1, data.BannerData, opt)
		if err != nil {
			return err
		}
		_, err = r.createBannerTags(ctx, bannerVersion.ID, data.TagIDs, opt)
		if err != nil {
			return err
		}

		_, err = currBann.Update().SetIsActive(*data.IsActive).SetActiveVersionID(bannerVersionID).SetUpdatedAt(bannerVersion.CreatedAt).SetLatestVersionID(bannerVersionID).Save(ctx)
		if err != nil {
			return err
		}
		d := bannerVersion.Version - versCnt
		if d > 0 {
			err = r.DeleteVersion(ctx, *data.ID, d, opt)
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return err
	}

	return err
}

func (r *repository) UpdateActiveVersion(ctx context.Context, data *model.UpdateActiveVersion) error {
	_, err := r.client.Banner.UpdateOneID(*data.ID).SetActiveVersionID(data.ActiveID).Save(ctx)
	if err != nil {
		return err
	}
	return nil
}

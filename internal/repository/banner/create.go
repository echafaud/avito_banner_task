package banner

import (
	"avito/internal/ent"
	"avito/internal/model"
	"context"
	"github.com/google/uuid"
)

func (r *repository) Create(ctx context.Context, data *model.BannerData) (int, error) {
	id := 0
	err := r.WithTx(ctx, func(tx *ent.Tx) error {
		client := tx.Client()
		opt := Options{Tx: client}
		bannerVersionID := uuid.New()
		cBann, err := r.createBanner(ctx, bannerVersionID, data, opt)
		id = cBann.ID
		if err != nil {
			return err
		}
		bannerVersion, err := r.createBannerVersion(ctx, cBann.ID, bannerVersionID, 1, data, opt)
		if err != nil {
			return err
		}
		_, err = r.createBannerTags(ctx, bannerVersion.ID, data.TagIDs, opt)
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (r *repository) createBannerVersion(ctx context.Context, bannerID int, bannerVersionID uuid.UUID, version int, data *model.BannerData, options Options) (*ent.BannerVersion, error) {
	client := r.client
	if options.Tx != nil {
		client = options.Tx
	}
	newBannerVersion, err := client.BannerVersion.
		Create().
		SetID(bannerVersionID).
		SetContent(data.Content).
		SetVersion(version).
		SetBannerID(bannerID).
		SetFeatureID(*data.FeatureID).
		Save(ctx)

	return newBannerVersion, err
}

func (r *repository) createBannerTags(ctx context.Context, versionID uuid.UUID, tags []int, options Options) ([]*ent.BannerVersionTag, error) {
	client := r.client
	if options.Tx != nil {
		client = options.Tx
	}
	bannerTags, err := client.BannerVersionTag.MapCreateBulk(tags, func(c *ent.BannerVersionTagCreate, i int) {
		c.SetTagID(tags[i]).SetBannerVersionID(versionID)
	}).Save(ctx)

	return bannerTags, err
}

func (r *repository) createBanner(ctx context.Context, bannerVersionID uuid.UUID, data *model.BannerData, options Options) (*ent.Banner, error) {
	client := r.client
	if options.Tx != nil {
		client = options.Tx
	}
	newBanner, err := client.Banner.
		Create().
		SetLatestVersionID(bannerVersionID).
		SetIsActive(*data.IsActive).
		SetActiveVersionID(bannerVersionID).
		Save(ctx)
	return newBanner, err
}

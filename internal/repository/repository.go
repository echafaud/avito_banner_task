package repository

import (
	"avito/internal/ent"
	"avito/internal/model"
	"avito/internal/repository/banner"
	"context"
	"github.com/google/uuid"
)

type BannerRepository interface {
	Create(ctx context.Context, data *model.BannerData) (int, error)
	Get(ctx context.Context, featureID int, tagID int) (*ent.BannerVersion, error)
	GetMainBanner(ctx context.Context, id int) (*ent.Banner, error)
	GetVersionBanner(ctx context.Context, id uuid.UUID) (*ent.BannerVersion, error)
	GetVersions(ctx context.Context, id int) ([]*ent.BannerVersion, error)
	Update(ctx context.Context, currBann *ent.Banner, data *model.BannerUpdate, currVers int, versCnt int) error
	UpdateActiveVersion(ctx context.Context, data *model.UpdateActiveVersion) error
	Delete(ctx context.Context, id int) error
	GetByFilter(ctx context.Context, filter *model.BannerGetByFilter) ([]*model.Banner, error)
	Exists(ctx context.Context, featureID int, tagIDs []int, opt *banner.ExistsOpt) (bool, error)
}

package validator

import (
	customError "avito/internal/error/banner"
	"github.com/go-playground/validator/v10"
	"reflect"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	if err := cv.validator.Struct(i); err != nil {
		return customError.ToCustomErrorFromValidator(err.(validator.ValidationErrors)[0])
	}
	return nil
}

func New() *CustomValidator {
	return &CustomValidator{
		validator: validator.New(),
	}
}

func (cv *CustomValidator) RegisterValidation(name string, fn func(fl validator.FieldLevel) bool) error {
	err := cv.validator.RegisterValidation(name, fn)
	if err != nil {
		return err
	}
	return nil
}

func NestedAllRequired(fl validator.FieldLevel) bool {
	banner := fl.Field().Interface()
	f := reflect.ValueOf(banner)
	for i := 0; i < f.NumField(); i++ {
		if f.Field(i).IsNil() {
			return false
		}
	}
	return true
}

func NestedAnyRequired(fl validator.FieldLevel) bool {
	banner := fl.Field().Interface()
	f := reflect.ValueOf(banner)
	for i := 0; i < f.NumField(); i++ {
		if !f.Field(i).IsNil() {
			return true
		}
	}
	return false
}

package banner

import (
	customError "avito/internal/error"
	"avito/internal/model"
	"github.com/labstack/echo/v4"
	"net/http"
)

func (a *api) Update(c echo.Context) error {
	bannerSchema := new(model.BannerUpdate)
	if err := c.Bind(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	if err := c.Validate(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	err := a.userService.Update(c.Request().Context(), bannerSchema)
	if err != nil {
		return customError.NewHTTPError(c, err)
	}

	return c.NoContent(http.StatusOK)
}

func (a *api) UpdateActiveVersion(c echo.Context) error {
	bannerSchema := new(model.UpdateActiveVersion)
	if err := c.Bind(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	if err := c.Validate(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	err := a.userService.UpdateActiveVersion(c.Request().Context(), bannerSchema)
	if err != nil {
		return customError.NewHTTPError(c, err)
	}
	return c.NoContent(http.StatusOK)
}

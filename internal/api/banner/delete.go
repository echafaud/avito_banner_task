package banner

import (
	customError "avito/internal/error"
	"avito/internal/model"
	"github.com/labstack/echo/v4"
	"net/http"
)

func (a *api) Delete(c echo.Context) error {
	bannerSchema := new(model.BannerDelete)
	if err := c.Bind(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	if err := c.Validate(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	err := a.userService.Delete(c.Request().Context(), *bannerSchema.ID)
	if err != nil {
		return customError.NewHTTPError(c, err)
	}
	return c.NoContent(http.StatusNoContent)
}

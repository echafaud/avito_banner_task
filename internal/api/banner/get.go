package banner

import (
	authProvider "avito/internal/auth/provider"
	customError "avito/internal/error"
	bannerError "avito/internal/error/banner"
	"avito/internal/model"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"net/http"
	"time"
)

func (a *api) Get(c echo.Context) error {
	bannerSchema := new(model.BannerGet)
	if err := c.Bind(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	if err := c.Validate(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	key := fmt.Sprintf("%v.%v", *bannerSchema.Banner.FeatureID, *bannerSchema.Banner.TagID)
	ctx := c.Request().Context()
	if !bannerSchema.UseLastVersion {
		res, err := a.redis.Get(ctx, key).Result()
		if err == nil {
			var content map[string]interface{}
			err = json.Unmarshal([]byte(res), &content)
			if err != nil {
				return customError.NewHTTPError(c, bannerError.InternalError)
			}
			return c.JSON(http.StatusOK, content)
		}
	}

	content, err := a.userService.Get(ctx, *bannerSchema.Banner.FeatureID, *bannerSchema.Banner.TagID)
	if err != nil {
		return customError.NewHTTPError(c, err)
	}
	defer func() {
		if content != nil {
			jsonContent, _ := json.Marshal(content)
			_ = a.redis.Set(ctx, key, jsonContent, 5*time.Minute).Err()
			//log if err
			//}
		}
	}()

	return c.JSON(http.StatusOK, content)
}

func (a *api) GetByFilter(c echo.Context) error {
	bannerSchema := new(model.BannerGetByFilter)
	if err := c.Bind(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	if err := c.Validate(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	banners, err := a.userService.GetByFilter(c.Request().Context(), bannerSchema)
	if err != nil {
		return customError.NewHTTPError(c, err)
	}
	return c.JSON(http.StatusOK, banners)
}

func (a *api) GetVersions(c echo.Context) error {
	bannerSchema := new(model.BannerDelete)
	if err := c.Bind(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	if err := c.Validate(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	banners, err := a.userService.GetVersions(c.Request().Context(), *bannerSchema.ID)
	if err != nil {
		return customError.NewHTTPError(c, err)
	}
	return c.JSON(http.StatusOK, banners)
}

func (a *api) GetUserToken(c echo.Context) error {
	userClaims := authProvider.UserClaims{ID: uuid.New(), IsAdmin: false}
	token, err := a.auth.JwtToken(userClaims, "user", 24*time.Hour)
	if err != nil {
		return customError.NewHTTPError(c, bannerError.InternalError)
	}
	return c.JSON(http.StatusOK, echo.Map{"token": token})
}

func (a *api) GetAdminToken(c echo.Context) error {
	userClaims := authProvider.UserClaims{ID: uuid.New(), IsAdmin: true}
	token, err := a.auth.JwtToken(userClaims, "admin", 24*time.Hour)
	if err != nil {
		return customError.NewHTTPError(c, bannerError.InternalError)
	}
	return c.JSON(http.StatusOK, echo.Map{"token": token})
}

package banner

import (
	customError "avito/internal/error"
	"avito/internal/model"
	"github.com/labstack/echo/v4"
	"net/http"
)

func (a *api) Create(c echo.Context) error {
	bannerSchema := new(model.BannerCreate)
	if err := c.Bind(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	if err := c.Validate(bannerSchema); err != nil {
		return customError.NewHTTPError(c, err)
	}
	bannerID, err := a.userService.Create(c.Request().Context(), bannerSchema.BannerData)
	if err != nil {
		return customError.NewHTTPError(c, err)
	}
	return c.JSON(http.StatusCreated, echo.Map{"banner_id": bannerID})
}

package banner

import (
	"avito/internal/auth"
	"avito/internal/service"
	"github.com/redis/go-redis/v9"
)

type api struct {
	userService service.BannerService
	redis       *redis.Client
	auth        auth.Provider
}

func New(userService service.BannerService, redisClient *redis.Client, auth auth.Provider) *api {
	return &api{
		userService: userService,
		redis:       redisClient,
		auth:        auth,
	}
}

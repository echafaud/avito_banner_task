package api

import (
	"github.com/labstack/echo/v4"
)

type BannerAPI interface {
	Create(c echo.Context) error
	Get(c echo.Context) error
	GetByFilter(c echo.Context) error
	GetUserToken(c echo.Context) error
	GetAdminToken(c echo.Context) error
	GetVersions(c echo.Context) error
	Update(c echo.Context) error
	UpdateActiveVersion(c echo.Context) error
	Delete(c echo.Context) error
}

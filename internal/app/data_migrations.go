package app

import (
	"ariga.io/atlas/sql/migrate"
	"context"
	"entgo.io/ent/dialect"
	"entgo.io/ent/dialect/sql/schema"
	"fmt"
)

func FillTags(tagsCnt int, featureCnt int) func(next schema.Applier) schema.Applier {
	return func(next schema.Applier) schema.Applier {
		return schema.ApplyFunc(func(ctx context.Context, conn dialect.ExecQuerier, plan *migrate.Plan) error {

			plan.Changes = append(plan.Changes, []*migrate.Change{
				{
					Cmd: fmt.Sprintf("INSERT INTO tags (id) SELECT t.id FROM generate_series(1, %d) AS t (id) ON CONFLICT DO NOTHING;", tagsCnt),
				},
				{
					Cmd: fmt.Sprintf("INSERT INTO features (id) SELECT f.id FROM generate_series(1, %d) AS f (id) ON CONFLICT DO NOTHING;", featureCnt),
				},
			}...)

			return next.Apply(ctx, conn, plan)
		})
	}
}

package app

import (
	"avito/internal/api"
	"avito/internal/api/banner"
	"avito/internal/auth"
	authProvider "avito/internal/auth/provider"
	"avito/internal/config"
	"avito/internal/ent"
	"avito/internal/repository"
	bannerRepository "avito/internal/repository/banner"
	"avito/internal/service"
	bannerService "avito/internal/service/banner"
	"avito/internal/validator"
	"context"
	"entgo.io/ent/dialect/sql/schema"
	"errors"
	"fmt"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
	"net/http"
)

type App struct {
	dbClient *ent.Client

	redisClient *redis.Client

	config *config.Config

	bannerRepository repository.BannerRepository

	bannerService service.BannerService

	api api.BannerAPI

	server *echo.Echo

	auth auth.Provider
}

func New(ctx context.Context, configName string, configType string, configPath string) (*App, error) {
	var err error
	a := &App{}

	a.config, err = a.initConfig(ctx, configName, configType, configPath)
	if err != nil {
		return nil, err
	}
	a.dbClient, err = a.initDBClient(ctx)
	if err != nil {
		return nil, err
	}
	a.redisClient, err = a.initRedisClient(ctx)
	if err != nil {
		return nil, err
	}
	a.bannerRepository, err = a.initBannerRepository(ctx)
	if err != nil {
		return nil, err
	}
	a.auth, err = a.initAuth(ctx)
	if err != nil {
		return nil, err
	}
	a.bannerService, err = a.initBannerService(ctx)
	if err != nil {
		return nil, err
	}
	a.api, err = a.initAPI(ctx)
	if err != nil {
		return nil, err
	}
	a.server, err = a.initServer(ctx)
	if err != nil {
		return nil, err
	}
	return a, nil
}

func (a *App) Run() error {
	if err := a.server.Start(a.config.Serv.Address); !errors.Is(err, http.ErrServerClosed) {
		return err
	}
	return nil
}

func (a *App) Server() *echo.Echo {
	return a.server
}

func (a *App) Auth() auth.Provider {
	return a.auth
}
func (a *App) BannerService() service.BannerService {
	return a.bannerService
}

func (a *App) DBClient() *ent.Client {
	return a.dbClient
}

func (a *App) Redis() *redis.Client {
	return a.redisClient
}
func (a *App) initConfig(_ context.Context, configName string, configType string, configPath string) (*config.Config, error) {
	config.Init(configName, configType, configPath)
	conf, err := config.New()
	if err != nil {
		return nil, err
	}
	return conf, nil
}

func (a *App) initServer(ctx context.Context) (*echo.Echo, error) {
	server := echo.New()
	if err := a.initEndpoints(ctx, server); err != nil {
		return nil, err
	}
	if err := a.initValidator(ctx, server); err != nil {
		return nil, err
	}
	return server, nil
}

func (a *App) initEndpoints(_ context.Context, server *echo.Echo) error {
	userRoutes := server.Group("/user_banner", echojwt.WithConfig(a.auth.SetupMW(false)))
	adminRoutes := server.Group("/banner", echojwt.WithConfig(a.auth.SetupMW(true)))
	tokenRoutes := server.Group("/token")
	tokenRoutes.GET("/user", a.api.GetUserToken)
	tokenRoutes.GET("/admin", a.api.GetAdminToken)
	userRoutes.GET("", a.api.Get)
	adminRoutes.POST("", a.api.Create)
	adminRoutes.GET("", a.api.GetByFilter)
	adminRoutes.PATCH("/:id", a.api.Update)
	adminRoutes.PATCH("/:id/version/:version_id", a.api.UpdateActiveVersion)
	adminRoutes.GET("/:id/version", a.api.GetVersions)
	adminRoutes.DELETE("/:id", a.api.Delete)
	return nil
}

func (a *App) initValidator(_ context.Context, server *echo.Echo) error {
	v := validator.New()
	if err := v.RegisterValidation("nestedAllRequired", validator.NestedAllRequired); err != nil {
		return err
	}
	if err := v.RegisterValidation("nestedAnyRequired", validator.NestedAnyRequired); err != nil {
		return err
	}
	server.Validator = v
	return nil
}

func (a *App) initDBClient(ctx context.Context) (*ent.Client, error) {
	client, err := ent.Open("postgres",
		fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
			a.config.Db.Host,
			a.config.Db.Port,
			a.config.Db.User,
			a.config.Db.Name,
			a.config.Db.Password,
		))
	if err != nil {
		return nil, err
	}
	if err = client.Schema.Create(ctx, schema.WithApplyHook(FillTags(a.config.DbData.TagsCnt, a.config.DbData.FeaturesCnt))); err != nil {
		return nil, err
	}

	return client, nil
}

func (a *App) initRedisClient(ctx context.Context) (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{Addr: a.config.Redis.Address})
	_, err := client.Ping(ctx).Result()
	if err != nil {
		return nil, err
	}
	return client, nil
}

func (a *App) initBannerRepository(_ context.Context) (repository.BannerRepository, error) {
	return bannerRepository.New(a.dbClient), nil
}

func (a *App) initBannerService(_ context.Context) (service.BannerService, error) {
	return bannerService.New(a.bannerRepository), nil
}

func (a *App) initAPI(_ context.Context) (api.BannerAPI, error) {
	return banner.New(a.bannerService, a.redisClient, a.auth), nil
}

func (a *App) initAuth(_ context.Context) (auth.Provider, error) {
	conf := authProvider.Config(a.config.Auth)
	return authProvider.New(conf), nil
}

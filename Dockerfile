FROM golang:latest

RUN mkdir /avito

WORKDIR /avito

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build -o main ./cmd/main.go

RUN chmod +x /avito/*.sh

ENTRYPOINT /avito/init.sh
